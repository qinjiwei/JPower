package com.wlcb.jpower.module.common.utils.constants;

/**
 * @ClassName ConstantsUtils
 * @Description TODO
 * @Author 郭丁志
 * @Date 2020-03-28 22:16
 * @Version 1.0
 */
public class ConstantsUtils {

    /** 默認文件加密key **/
    public static final String FILE_DES_KEY = "COREFILEENCRYPTKEY20200720";
    /** 用戶默認密碼。這裏的優先級最低 **/
    public static final String DEFAULT_USER_PASSWORD = "123456";
    /** 用戶默認是否激活。這裏的優先級最低 **/
    public static final Integer DEFAULT_USER_ACTIVATION = ConstantsEnum.YN01.N.getValue();

}
