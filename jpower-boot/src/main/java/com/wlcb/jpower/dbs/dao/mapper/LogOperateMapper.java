package com.wlcb.jpower.dbs.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.dbs.entity.TbLogOperate;
import org.springframework.stereotype.Component;

/**
 * @Author mr.g
 * @Date 2021/5/1 0001 19:39
 */
@Component
public interface LogOperateMapper extends BaseMapper<TbLogOperate> {
}
